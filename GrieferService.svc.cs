﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.IO;
using System.Configuration;
using System.Xml;
using System.Xml.Linq;
using System.ServiceModel.Web;

namespace Vudu.PublicServices
{
    public class GrieferService : IGrieferService
    {
        public Stream CheckKeys(Stream keys)
        {
            // Set up WCF service response type, and read incoming data.
            WebOperationContext.Current.OutgoingResponse.ContentType = "text/html";
            StreamReader reader = new StreamReader(keys);
            String res = reader.ReadToEnd();
            reader.Close();
            reader.Dispose();

            // Parse incoming data into an array of Guid's (SL Keys).
            Guid[] keysToCheck = res.Split(new String[] { "," }, StringSplitOptions.RemoveEmptyEntries)
                                    .Select(k => new Guid(k))
                                    .ToArray();

            // If theres no incoming data, we're missing the auth token, so quit.
            if (keysToCheck.Length < 2)
                return new MemoryStream(Encoding.Default.GetBytes("err,Error."));

            // The first key in the array, is the auth token, so save that seperately and remove it from the array.
            Guid authToken = keysToCheck[0];
            keysToCheck = keysToCheck.Where(k => k != authToken).ToArray();

            // The 2nd key in the array, is the owner key, so save that seperately and remove it from the array.
            Guid ownerKey = keysToCheck[1];
            keysToCheck = keysToCheck.Where(k => k != ownerKey).ToArray();

            if (isBlackListed(ownerKey))
                return new MemoryStream(Encoding.Default.GetBytes("null"));

            // Check the auth token is valid, and return a message if not.
            if (!isAuthTokenValid(authToken.ToString()))
                return new MemoryStream(Encoding.Default.GetBytes("err,Authorization token was invalid."));

            // Load the ban list from an XML file.
            String listPath = ConfigurationManager.AppSettings["GrieferListPath"];
            var xml = XDocument.Load(listPath);

            // Loop through all the keys passed in, to see if any are on the XML list.
            StringBuilder response = new StringBuilder();
            foreach (var k in keysToCheck)
            {
                // Try to get a ban record from the XML, based on the current key iteration.
                var griefer = xml.Descendants("Griefer")
                                 .Where(g => new Guid(g.Attribute("key").Value) == k)
                                 .FirstOrDefault();
                if (!isWhiteListed(k))
                {
                    // If we managed to find the key in the ban list, and the data to the response.
                    if (griefer != null)
                    {
                        if (response.Length > 0)
                            response.Append("," + griefer.Attribute("key").Value + "," +
                                                  griefer.Attribute("name").Value);
                        else
                            response.Append("inf," + griefer.Attribute("key").Value + "," +
                                            griefer.Attribute("name").Value);
                    }
                }
            }
            if (response.Length == 0)
                response.Append("null");

            // Return all the keys passed in, that were also found in the ban list.
            return new MemoryStream(Encoding.Default.GetBytes(response.ToString()));
        }

        public Stream AddGriefer(Stream info)
        {
            // Set up WCF service response type, and read incoming data.
            WebOperationContext.Current.OutgoingResponse.ContentType = "text/html";
            Guid ownerKey = Guid.Empty;
            Guid.TryParse(WebOperationContext.Current.IncomingRequest.Headers["X-SecondLife-Owner-Key"], out ownerKey);
            if (isBlackListed(ownerKey))
                return new MemoryStream(Encoding.Default.GetBytes("err,You are blacklisted"));

            StreamReader reader = new StreamReader(info);
            String res = reader.ReadToEnd();
            reader.Close();
            reader.Dispose();

            // Parse the incoming data and ensure we have enough parameters passed in.
            String[] incoming = res.Split(new String[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToArray();
            if(incoming.Length < 5)
                return new MemoryStream(Encoding.Default.GetBytes("err,Error."));

            Guid.TryParse(incoming[3], out ownerKey);
            if (isBlackListed(ownerKey))
                return new MemoryStream(Encoding.Default.GetBytes("err,You are blacklisted"));

            // Check the auth token is valid, and return a message if not.
            if (!isAuthTokenValid(incoming[0]))
                return new MemoryStream(Encoding.Default.GetBytes("err,Authorization token was invalid."));

            // Load the griefer list from an XML file.
            XDocument xml = XDocument.Load(ConfigurationManager.AppSettings["GrieferListPath"]);
            var existing = (from a in xml.Descendants("Griefer")
                            select a).ToArray();

            // If we found an existing record, no action is needed so return a message.
            if(existing.Where(e => e.Attribute("key").Value.ToLower() == incoming[1]).Count() > 0)
                return new MemoryStream(Encoding.Default.GetBytes("err,Griefer already exists."));

            if(isWhiteListed(new Guid(incoming[1])))
                return new MemoryStream(Encoding.Default.GetBytes("err," + incoming[2] + " cannot be banned."));

            // Create an XML writer to update the griefer list file.
            using (XmlWriter writer = XmlWriter.Create(ConfigurationManager.AppSettings["GrieferListPath"]))
            {
                // Root XML element
                writer.WriteStartDocument();
                writer.WriteStartElement("Griefers");

                // Loop through existing griefer records, writing them to the new file.
                foreach (var e in existing)
                {

                    writer.WriteStartElement("Griefer");
                    writer.WriteAttributeString("name", e.Attribute("name").Value);
                    writer.WriteAttributeString("key", e.Attribute("key").Value);
                    writer.WriteAttributeString("addedbyname", e.Attribute("addedbyname").Value);
                    writer.WriteAttributeString("addeddate", e.Attribute("addeddate").Value);
                    writer.WriteEndElement();
                }
                // Write the new griefer record to the end of the XML file.
                writer.WriteStartElement("Griefer");
                writer.WriteAttributeString("name", incoming[2]);
                writer.WriteAttributeString("key", incoming[1]);
                writer.WriteAttributeString("addedbyname", incoming[4]);
                writer.WriteAttributeString("addeddate", DateTime.Now.ToLongDateString());
                writer.WriteEndElement();

                writer.WriteEndElement();
                writer.WriteEndDocument();
            }

            Guid targetKey = new Guid(incoming[1]);
            addHistory(ownerKey, incoming[4], targetKey, incoming[2], "added to ban list");

            // Report that the ban was successful.
            return new MemoryStream(Encoding.Default.GetBytes("ok,Successfully added " + incoming[2] + " to the ban list."));
        }

        public Stream RemoveGriefer(Stream info)
        {
            // Set up WCF service response type, and read incoming data.
            WebOperationContext.Current.OutgoingResponse.ContentType = "text/html";
            Guid ownerKey = Guid.Empty;
            Guid.TryParse(WebOperationContext.Current.IncomingRequest.Headers["X-SecondLife-Owner-Key"], out ownerKey);
            if (isBlackListed(ownerKey))
                return new MemoryStream(Encoding.Default.GetBytes("err,You are blacklisted"));

            StreamReader reader = new StreamReader(info);
            String res = reader.ReadToEnd();
            reader.Close();
            reader.Dispose();

            // Parse the incoming data and ensure we have enough parameters passed in.
            String[] incoming = res.Split(new String[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToArray();
            if (incoming.Length < 5)
                return new MemoryStream(Encoding.Default.GetBytes("err,Error."));

            Guid.TryParse(incoming[3], out ownerKey);
            if (isBlackListed(ownerKey))
                return new MemoryStream(Encoding.Default.GetBytes("err,You are blacklisted"));

            // Check the auth token is valid, and return a message if not.
            if (!isAuthTokenValid(incoming[0]))
                return new MemoryStream(Encoding.Default.GetBytes("err,Authorization token was invalid."));

            // Load the ban list from an XML file.
            XDocument xml = XDocument.Load(ConfigurationManager.AppSettings["GrieferListPath"]);
            var existing = (from a in xml.Descendants("Griefer")
                            select a).ToArray();

            // If we didn't find an existing record, no action is needed so return a message.
            if (existing.Where(e => e.Attribute("key").Value.ToLower() == incoming[1].ToLower()).Count() == 0)
                return new MemoryStream(Encoding.Default.GetBytes("err,Avatar is not banned."));

            // Create an XML writer to update the ban list file.
            using (XmlWriter writer = XmlWriter.Create(ConfigurationManager.AppSettings["GrieferListPath"]))
            {
                // Root XML element
                writer.WriteStartDocument();
                writer.WriteStartElement("Griefers");

                // Loop through existing ban records, writing them to the new file.
                foreach (var e in existing)
                {
                    // If its not the one we're deleting, write it to the file.
                    if (e.Attribute("key").Value.ToLower() != incoming[1].ToLower())
                    {
                        writer.WriteStartElement("Griefer");
                        writer.WriteAttributeString("name", e.Attribute("name").Value);
                        writer.WriteAttributeString("key", e.Attribute("key").Value);
                        writer.WriteAttributeString("addedbyname", e.Attribute("addedbyname").Value);
                        writer.WriteAttributeString("addeddate", e.Attribute("addeddate").Value);
                        writer.WriteEndElement();
                    }
                }
                writer.WriteEndElement();
                writer.WriteEndDocument();
            }

            Guid targetKey = new Guid(incoming[1]);
            addHistory(ownerKey, incoming[4], targetKey, incoming[2], "removed from ban list");

            // Report that the ban was successful.
            return new MemoryStream(Encoding.Default.GetBytes("ok,Successfully deleted " + incoming[2] + " from the ban list."));
        }

        public Stream AddWhiteList(Stream info)
        {
            // Set up WCF service response type, and read incoming data.
            WebOperationContext.Current.OutgoingResponse.ContentType = "text/html";
            Guid ownerKey = Guid.Empty;
            Guid.TryParse(WebOperationContext.Current.IncomingRequest.Headers["X-SecondLife-Owner-Key"], out ownerKey);
            String ownerName = WebOperationContext.Current.IncomingRequest.Headers["X-SecondLife-Owner-Name"];
            if (isBlackListed(ownerKey))
                return new MemoryStream(Encoding.Default.GetBytes("err,You are blacklisted"));

            StreamReader reader = new StreamReader(info);
            String res = reader.ReadToEnd();
            reader.Close();
            reader.Dispose();

            // Parse the incoming data and ensure we have enough parameters passed in.
            String[] incoming = res.Split(new String[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToArray();
            if (incoming.Length < 3)
                return new MemoryStream(Encoding.Default.GetBytes("err,Error."));

            // Check the auth token is valid, and return a message if not.
            if (!isAuthTokenValid(incoming[0]))
                return new MemoryStream(Encoding.Default.GetBytes("err,Authorization token was invalid."));

            // Load the whitelist from an XML file.
            XDocument xml = XDocument.Load(ConfigurationManager.AppSettings["WhiteListPath"]);
            var existing = (from a in xml.Descendants("WhiteListed")
                            select a).ToArray();

            // If we found an existing record, no action is needed so return a message.
            if (existing.Where(e => e.Attribute("key").Value.ToLower() == incoming[1]).Count() > 0)
                return new MemoryStream(Encoding.Default.GetBytes("err,Avatar is already globally whitelisted."));

            // Create an XML writer to update the whitelist file.
            using (XmlWriter writer = XmlWriter.Create(ConfigurationManager.AppSettings["WhiteListPath"]))
            {
                // Root XML element
                writer.WriteStartDocument();
                writer.WriteStartElement("Unbannable");

                // Loop through existing whitelist records, writing them to the new file.
                foreach (var e in existing)
                {

                    writer.WriteStartElement("WhiteListed");
                    writer.WriteAttributeString("name", e.Attribute("name").Value);
                    writer.WriteAttributeString("key", e.Attribute("key").Value);
                    writer.WriteAttributeString("addedbyname", e.Attribute("addedbyname").Value);
                    writer.WriteAttributeString("addeddate", e.Attribute("addeddate").Value);
                    writer.WriteEndElement();
                }
                // Write the new whitelist record to the end of the XML file.
                writer.WriteStartElement("WhiteListed");
                writer.WriteAttributeString("name", incoming[2]);
                writer.WriteAttributeString("key", incoming[1]);
                writer.WriteAttributeString("addedbyname", ownerName);
                writer.WriteAttributeString("addeddate", DateTime.Now.ToLongDateString());
                writer.WriteEndElement();

                writer.WriteEndElement();
                writer.WriteEndDocument();
            }

            Guid targetKey = new Guid(incoming[1]);
            addHistory(ownerKey, ownerName, targetKey, incoming[2], "added to white list");

            // Report that the avatar was whitelisted successfully.
            return new MemoryStream(Encoding.Default.GetBytes("ok,Successfully whitelisted " + incoming[2] + "."));
        }

        public Stream RemoveWhiteList(Stream info)
        {
            // Set up WCF service response type, and read incoming data.
            WebOperationContext.Current.OutgoingResponse.ContentType = "text/html";
            Guid ownerKey = Guid.Empty;
            Guid.TryParse(WebOperationContext.Current.IncomingRequest.Headers["X-SecondLife-Owner-Key"], out ownerKey);
            if (isBlackListed(ownerKey))
                return new MemoryStream(Encoding.Default.GetBytes("err,You are blacklisted"));
            String ownerName = WebOperationContext.Current.IncomingRequest.Headers["X-SecondLife-Owner-Name"];

            StreamReader reader = new StreamReader(info);
            String res = reader.ReadToEnd();
            reader.Close();
            reader.Dispose();

            // Parse the incoming data and ensure we have enough parameters passed in.
            String[] incoming = res.Split(new String[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToArray();
            if (incoming.Length < 3)
                return new MemoryStream(Encoding.Default.GetBytes("err,Error."));

            // Check the auth token is valid, and return a message if not.
            if (!isAuthTokenValid(incoming[0]))
                return new MemoryStream(Encoding.Default.GetBytes("err,Authorization token was invalid."));

            // Load the whitelist list from an XML file.
            XDocument xml = XDocument.Load(ConfigurationManager.AppSettings["WhiteListPath"]);
            var existing = (from a in xml.Descendants("WhiteListed")
                            select a).ToArray();

            // If we didn't find an existing record, no action is needed so return a message.
            if (existing.Where(e => e.Attribute("key").Value.ToLower() == incoming[1].ToLower()).Count() == 0)
                return new MemoryStream(Encoding.Default.GetBytes("err,Avatar is not whitelisted."));

            // Create an XML writer to update the whitelist list file.
            using (XmlWriter writer = XmlWriter.Create(ConfigurationManager.AppSettings["WhiteListPath"]))
            {
                // Root XML element
                writer.WriteStartDocument();
                writer.WriteStartElement("Unbannable");

                // Loop through existing whitelist records, writing them to the new file.
                foreach (var e in existing)
                {
                    // If its not the one we're deleting, write it to the file.
                    if (e.Attribute("key").Value.ToLower() != incoming[1].ToLower())
                    {
                        writer.WriteStartElement("WhiteListed");
                        writer.WriteAttributeString("name", e.Attribute("name").Value);
                        writer.WriteAttributeString("key", e.Attribute("key").Value);
                        writer.WriteAttributeString("addedbyname", e.Attribute("addedbyname").Value);
                        writer.WriteAttributeString("addeddate", e.Attribute("addeddate").Value);
                        writer.WriteEndElement();
                    }
                }
                writer.WriteEndElement();
                writer.WriteEndDocument();
            }

            Guid targetKey = new Guid(incoming[1]);
            addHistory(ownerKey, ownerName, targetKey, incoming[2], "removed from white list");

            // Report that the whitelist removal was successful.
            return new MemoryStream(Encoding.Default.GetBytes("ok,Successfully deleted " + incoming[2] + " from the global whitelist."));
        }

        public Stream AddOwnerBlackList(Stream info)
        {
            // Set up WCF service response type, and read incoming data.
            WebOperationContext.Current.OutgoingResponse.ContentType = "text/html";
            Guid ownerKey = Guid.Empty;
            Guid.TryParse(WebOperationContext.Current.IncomingRequest.Headers["X-SecondLife-Owner-Key"], out ownerKey);
            String ownerName = WebOperationContext.Current.IncomingRequest.Headers["X-SecondLife-Owner-Name"];
            if (isBlackListed(ownerKey))
                return new MemoryStream(Encoding.Default.GetBytes("err,You are blacklisted"));

            StreamReader reader = new StreamReader(info);
            String res = reader.ReadToEnd();
            reader.Close();
            reader.Dispose();

            // Parse the incoming data and ensure we have enough parameters passed in.
            String[] incoming = res.Split(new String[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToArray();
            if (incoming.Length < 3)
                return new MemoryStream(Encoding.Default.GetBytes("err,Error."));

            // Check the auth token is valid, and return a message if not.
            if (!isAuthTokenValid(incoming[0]))
                return new MemoryStream(Encoding.Default.GetBytes("err,Authorization token was invalid."));

            // Load the owner blacklist from an XML file.
            XDocument xml = XDocument.Load(ConfigurationManager.AppSettings["OwnerBlackListPath"]);
            var existing = (from a in xml.Descendants("BlackListed")
                            select a).ToArray();

            // If we found an existing record, no action is needed so return a message.
            if (existing.Where(e => e.Attribute("key").Value.ToLower() == incoming[1]).Count() > 0)
                return new MemoryStream(Encoding.Default.GetBytes("err,Avatar is already blacklisted."));

            // Create an XML writer to update the owner blacklist file.
            using (XmlWriter writer = XmlWriter.Create(ConfigurationManager.AppSettings["OwnerBlackListPath"]))
            {
                // Root XML element
                writer.WriteStartDocument();
                writer.WriteStartElement("Banned");

                // Loop through existing owner blacklist records, writing them to the new file.
                foreach (var e in existing)
                {

                    writer.WriteStartElement("BlackListed");
                    writer.WriteAttributeString("name", e.Attribute("name").Value);
                    writer.WriteAttributeString("key", e.Attribute("key").Value);
                    writer.WriteAttributeString("addedbyname", e.Attribute("addedbyname").Value);
                    writer.WriteAttributeString("addeddate", e.Attribute("addeddate").Value);
                    writer.WriteEndElement();
                }
                // Write the new owner blacklist record to the end of the XML file.
                writer.WriteStartElement("BlackListed");
                writer.WriteAttributeString("name", incoming[2]);
                writer.WriteAttributeString("key", incoming[1]);
                writer.WriteAttributeString("addedbyname", ownerName);
                writer.WriteAttributeString("addeddate", DateTime.Now.ToLongDateString());
                writer.WriteEndElement();

                writer.WriteEndElement();
                writer.WriteEndDocument();
            }

            Guid targetKey = new Guid(incoming[1]);
            addHistory(ownerKey, ownerName, targetKey, incoming[2], "added to owner blacklist");

            // Report that the avatar was blacklisted successfully.
            return new MemoryStream(Encoding.Default.GetBytes("ok,Successfully blacklisted " + incoming[2] + "."));
        }

        public Stream RemoveOwnerBlackList(Stream info)
        {
            // Set up WCF service response type, and read incoming data.
            WebOperationContext.Current.OutgoingResponse.ContentType = "text/html";
            Guid ownerKey = Guid.Empty;
            Guid.TryParse(WebOperationContext.Current.IncomingRequest.Headers["X-SecondLife-Owner-Key"], out ownerKey);
            if (isBlackListed(ownerKey))
                return new MemoryStream(Encoding.Default.GetBytes("err,You are blacklisted"));

            String ownerName = WebOperationContext.Current.IncomingRequest.Headers["X-SecondLife-Owner-Name"];

            StreamReader reader = new StreamReader(info);
            String res = reader.ReadToEnd();
            reader.Close();
            reader.Dispose();

            // Parse the incoming data and ensure we have enough parameters passed in.
            String[] incoming = res.Split(new String[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToArray();
            if (incoming.Length < 3)
                return new MemoryStream(Encoding.Default.GetBytes("err,Error."));

            // Check the auth token is valid, and return a message if not.
            if (!isAuthTokenValid(incoming[0]))
                return new MemoryStream(Encoding.Default.GetBytes("err,Authorization token was invalid."));

            // Load the owner blacklist from an XML file.
            XDocument xml = XDocument.Load(ConfigurationManager.AppSettings["OwnerBlackListPath"]);
            var existing = (from a in xml.Descendants("BlackListed")
                            select a).ToArray();

            // If we didn't find an existing record, no action is needed so return a message.
            if (existing.Where(e => e.Attribute("key").Value.ToLower() == incoming[1].ToLower()).Count() == 0)
                return new MemoryStream(Encoding.Default.GetBytes("err,Avatar is not blacklisted."));

            // Create an XML writer to update the owner blacklist file.
            using (XmlWriter writer = XmlWriter.Create(ConfigurationManager.AppSettings["OwnerBlackListPath"]))
            {
                // Root XML element
                writer.WriteStartDocument();
                writer.WriteStartElement("Banned");

                // Loop through existing owner blacklist records, writing them to the new file.
                foreach (var e in existing)
                {
                    // If its not the one we're deleting, write it to the file.
                    if (e.Attribute("key").Value.ToLower() != incoming[1].ToLower())
                    {
                        writer.WriteStartElement("BlackListed");
                        writer.WriteAttributeString("name", e.Attribute("name").Value);
                        writer.WriteAttributeString("key", e.Attribute("key").Value);
                        writer.WriteAttributeString("addedbyname", e.Attribute("addedbyname").Value);
                        writer.WriteAttributeString("addeddate", e.Attribute("addeddate").Value);
                        writer.WriteEndElement();
                    }
                }
                writer.WriteEndElement();
                writer.WriteEndDocument();
            }

            Guid targetKey = new Guid(incoming[1]);
            addHistory(ownerKey, ownerName, targetKey, incoming[2], "removed from owner blacklist");

            // Report that the whitelist removal was successful.
            return new MemoryStream(Encoding.Default.GetBytes("ok,Successfully deleted " + incoming[2] + " from the blacklist."));
        }

        public Stream ListCounts(Stream info)
        {
            // Set up WCF service response type, and read incoming data.
            WebOperationContext.Current.OutgoingResponse.ContentType = "text/html";
            Guid ownerKey = Guid.Empty;
            Guid.TryParse(WebOperationContext.Current.IncomingRequest.Headers["X-SecondLife-Owner-Key"], out ownerKey);
            if (isBlackListed(ownerKey))
                return new MemoryStream(Encoding.Default.GetBytes("err,You are blacklisted"));

            StreamReader reader = new StreamReader(info);
            String res = reader.ReadToEnd();
            reader.Close();
            reader.Dispose();

            StringBuilder response = new StringBuilder();
            response.Append("ok,");

            String grieferListPath = ConfigurationManager.AppSettings["GrieferListPath"];
            var grieferXml = XDocument.Load(grieferListPath);
            var banCount = (from g in grieferXml.Descendants("Griefer")
                            select g).Count();
            response.Append("Total Bans: " + banCount.ToString() + "\n");

            String whiteListPath = ConfigurationManager.AppSettings["WhiteListPath"];
            var whiteListXml = XDocument.Load(whiteListPath);
            var whiteListCount = (from g in whiteListXml.Descendants("WhiteListed")
                                  select g).Count();
            response.Append("Total White listed: " + whiteListCount.ToString() + "\n");

            String blackListPath = ConfigurationManager.AppSettings["OwnerBlackListPath"];
            var blackListXml = XDocument.Load(blackListPath);
            var blackListCount = (from g in blackListXml.Descendants("BlackListed")
                                  select g).Count();
            response.Append("Total Black listed: " + blackListCount.ToString() + "\n");

            return new MemoryStream(Encoding.Default.GetBytes(response.ToString()));
        }

        public Stream CheckUserStatus(Stream info)
        {
            // Set up WCF service response type, and read incoming data.
            WebOperationContext.Current.OutgoingResponse.ContentType = "text/html";
            Guid ownerKey = Guid.Empty;
            Guid.TryParse(WebOperationContext.Current.IncomingRequest.Headers["X-SecondLife-Owner-Key"], out ownerKey);
            if (isBlackListed(ownerKey))
                return new MemoryStream(Encoding.Default.GetBytes("err,You are blacklisted"));

            String ownerName = WebOperationContext.Current.IncomingRequest.Headers["X-SecondLife-Owner-Name"];

            StreamReader reader = new StreamReader(info);
            String res = reader.ReadToEnd();
            reader.Close();
            reader.Dispose();

            // Parse the incoming data and ensure we have enough parameters passed in.
            String[] incoming = res.Split(new String[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToArray();
            if (incoming.Length < 3)
                return new MemoryStream(Encoding.Default.GetBytes("err,Error."));

            // Check the auth token is valid, and return a message if not.
            if (!isAuthTokenValid(incoming[0]))
                return new MemoryStream(Encoding.Default.GetBytes("err,Authorization token was invalid."));

            // Load the ban list and try to find the avatar.
            String grieferListPath = ConfigurationManager.AppSettings["GrieferListPath"];
            var grieferXml = XDocument.Load(grieferListPath);
            var existingGriefer = (from g in grieferXml.Descendants("Griefer")
                                   where g.Attribute("key").Value == incoming[1]
                                   select g).FirstOrDefault();

            // Load the whitelist and try to find the avatar.
            String whiteListPath = ConfigurationManager.AppSettings["WhiteListPath"];
            var whiteListXml = XDocument.Load(whiteListPath);
            var existingWhiteList = (from g in whiteListXml.Descendants("WhiteListed")
                                   where g.Attribute("key").Value == incoming[1]
                                   select g).FirstOrDefault();

            // Load the owner blacklist and try to find the avatar.
            String blackListPath = ConfigurationManager.AppSettings["OwnerBlackListPath"];
            var blackListXml = XDocument.Load(blackListPath);
            var existingBlackList = (from g in blackListXml.Descendants("BlackListed")
                                     where g.Attribute("key").Value == incoming[1]
                                     select g).FirstOrDefault();

            StringBuilder response = new StringBuilder();
            response.Append("ok,");
            if (existingGriefer == null)
                response.Append("Not in the ban list.\n");
            else
            {
                DateTime addedDate = DateTime.Parse(existingGriefer.Attribute("addeddate").Value);
                response.Append("Added to the ban list by " + existingGriefer.Attribute("addedbyname").Value + " on " + addedDate.ToString("dd MMM yyyy") +".\n");
            }

            if (existingWhiteList == null)
                response.Append("Not in the global whitelist.\n");
            else
            {
                DateTime addedDate = DateTime.Parse(existingWhiteList.Attribute("addeddate").Value);
                response.Append("Added to the global whitelist by " + existingWhiteList.Attribute("addedbyname").Value + " on " + addedDate.ToString("dd MMM yyyy") + ".\n");
            }

            if (existingBlackList == null)
                response.Append("Not in the owner blacklist.\n");
            else
            {
                DateTime addedDate = DateTime.Parse(existingBlackList.Attribute("addeddate").Value);
                response.Append("Added to the owner blacklist by " + existingBlackList.Attribute("addedbyname").Value + " on " + addedDate.ToString("dd MMM yyyy") + ".\n");
            }

            var history = getHistory(new Guid(incoming[1]));
            foreach (var h in history)
            {
                DateTime logDate = DateTime.Parse(h.Attribute("logdate").Value);
                response.Append(h.Attribute("adminname").Value + " " +
                                h.Attribute("action").Value + " on " + 
                                logDate.ToString("dd MMM yyyy") + ".\n");
            }

            return new MemoryStream(Encoding.Default.GetBytes(response.ToString()));
        }

        // Check to see if the auth token is valid.
        private Boolean isAuthTokenValid(String token)
        {
            Guid authToken = new Guid(token);
            Guid validAuth = new Guid(ConfigurationManager.AppSettings["AuthToken"]);
            if (authToken != validAuth)
                return false;
            else
                return true;
        }

        // Check to see if an avatar key is whitelisted.
        private Boolean isWhiteListed(Guid avatarKey)
        {
            String whiteListPath = ConfigurationManager.AppSettings["WhiteListPath"];
            var whiteListXml = XDocument.Load(whiteListPath);
            var existingWhiteList = (from g in whiteListXml.Descendants("WhiteListed")
                                     where new Guid(g.Attribute("key").Value) == avatarKey
                                     select g).FirstOrDefault();

            if (existingWhiteList == null)
                return false;
            else
                return true;
        }

        // Check to see if an avatar key is blacklisted.
        private Boolean isBlackListed(Guid avatarKey)
        {
            // Load the owner blacklist and try to find the avatar.
            String blackListPath = ConfigurationManager.AppSettings["OwnerBlackListPath"];
            var blackListXml = XDocument.Load(blackListPath);
            var existingBlackList = (from g in blackListXml.Descendants("BlackListed")
                                     where new Guid(g.Attribute("key").Value) == avatarKey
                                     select g).FirstOrDefault();


            if (existingBlackList == null)
                return false;
            else
                return true;
        }

        private XElement[] getHistory(Guid avKey)
        {
            XDocument xml = XDocument.Load(ConfigurationManager.AppSettings["HistoryListPath"]);
            var existing = (from a in xml.Descendants("Action")
                            where new Guid(a.Attribute("targetkey").Value) == avKey
                            select a).ToArray();
            Int32 skip = existing.Length - 10;

            if(skip > 0)
                return existing.Skip(skip).ToArray();
            else
                return existing;
        }

        private void addHistory(Guid adminKey,
                                String adminName,
                                Guid targetKey,
                                String targetName,
                                String action)
        {
            XDocument xml = XDocument.Load(ConfigurationManager.AppSettings["HistoryListPath"]);
            var existing = (from a in xml.Descendants("Action")
                            select a).ToArray();

            // Create an XML writer to update the history file.
            using (XmlWriter writer = XmlWriter.Create(ConfigurationManager.AppSettings["HistoryListPath"]))
            {
                // Root XML element
                writer.WriteStartDocument();
                writer.WriteStartElement("History");

                // Loop through existing history records, writing them to the new file.
                foreach (var e in existing)
                {
                    writer.WriteStartElement("Action");
                    writer.WriteAttributeString("adminkey", e.Attribute("adminkey").Value);
                    writer.WriteAttributeString("adminname", e.Attribute("adminname").Value);
                    writer.WriteAttributeString("targetkey", e.Attribute("targetkey").Value);
                    writer.WriteAttributeString("targetname", e.Attribute("targetname").Value);
                    writer.WriteAttributeString("logdate", e.Attribute("logdate").Value);
                    writer.WriteAttributeString("action", e.Attribute("action").Value);
                    writer.WriteEndElement();
                }
                // Write the new history record to the end of the XML file.
                writer.WriteStartElement("Action");
                writer.WriteAttributeString("adminkey", adminKey.ToString());
                writer.WriteAttributeString("adminname", adminName);
                writer.WriteAttributeString("targetkey", targetKey.ToString());
                writer.WriteAttributeString("targetname", targetName);
                writer.WriteAttributeString("logdate", DateTime.Now.ToLongDateString());
                writer.WriteAttributeString("action", action);
                writer.WriteEndElement();

                writer.WriteEndElement();
                writer.WriteEndDocument();
            }
        }
    }
}
